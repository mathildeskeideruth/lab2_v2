'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

/**
 * Database structure
 *
 * /mad-app-errb
 *      /items
 *          /itemdId
 *              /interested_buyers
 *                  /buyer_id
 *
 *                      ... (buyer data)
 *
 *              /BOOL_SOLD
 *              /SELLER
 *              ... (rest of item data)
 *
 *      /users
 *          /userId
 *              /bought_items
 *                  /itemId
 *                      /itemId
 *
 *              /items
 *                  /itemId
 *                      /itemPath
 *
 *              /items_of_interest
 *                  /itemId
 *                      /itemId
 *
 *              /notificationTokens
 *                  /device_n_token
 *
 *              ... (user data)
 */

exports.sendSoldNotification = functions.firestore.document('/items/{itemId}')
  .onWrite(async (change, context) => {
    const itemId = context.params.itemId;

    // Get all data associated with the item
    const getItemDataPromise = admin.firestore()
    .doc(`items/${itemId}/`).get();
    const result = await getItemDataPromise;

    const sold = result.data()['BOOL_SOLD'];
    const seller = result.data()['SELLER']
    const title = result.data()['TITLE']

    // If item is not sold, do not send notifications
    if (!sold) {
      return console.log('Item', itemId, 'not sold.')
    }
    console.log('Item', itemId, 'sold!')
    
    // Get all interested users
    const getBuyersDataPromise = admin.firestore()
    .collection(`items/${itemId}/interested_buyers/`).get();
    const buyersResult = await getBuyersDataPromise;

    // Retrieve interested users IDs
    let buyersID = [];
    buyersResult.forEach(doc => buyersID.push(doc.id))

    let tokens = []
    let allTokensPromises = [];
  
    console.log('Number of buyers:', buyersID.length)
    for (let index = 0; index < buyersID.length; index++) {
      var buyer = buyersID[index]
      console.log(buyer)

      // Get all user tokens promises
      const getUserTokensPromise = admin.firestore()
      .collection(`/users/${buyer}/notificationTokens`).get();

      allTokensPromises.push(getUserTokensPromise)
    }

    const userTokensSnapshot = await Promise.all(allTokensPromises);
    for (let index = 0; index < allTokensPromises.length; index++) {
      userTokensSnapshot[index].forEach(doc => tokens.push(doc.id))
    }
    
    if (tokens.length === 0) {
      return console.log('There are no notification tokens to send to.');
    }
    console.log('There are', tokens.length, 'tokens to send notifications to.');
    

    // Notification details.
    const payload = {
      notification: {
        title: 'You bought successfully an item',
        body: `ou bought ${title}, congratulations!`
        // icon: follower.photoURL
      }
    };

    console.log(tokens)

    const response = await admin.messaging().sendToDevice(tokens, payload);
    // For each message check if there was an error.
    const tokensToRemove = [];
    response.results.forEach((result, index) => {
      const error = result.error;
      if (error) {
        console.error('Failure sending notification to', tokens[index], error);
        // Cleanup the tokens who are not registered anymore.
        if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
          tokensToRemove.push(tokens[index]);
        }
      }
    });
    return Promise.all(tokensToRemove);
  });