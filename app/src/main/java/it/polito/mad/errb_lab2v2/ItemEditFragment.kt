package it.polito.mad.errb_lab2v2

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.*
import androidx.activity.addCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.ktx.storage
import java.io.ByteArrayOutputStream
import java.util.*

class ItemEditFragment : Fragment() {

    private var itemId: String = ""

    private lateinit var itemInstance: Item

    private lateinit var titleField: EditText
    private lateinit var descField: EditText
    private lateinit var priceField: EditText
    private lateinit var locationField: EditText
    private lateinit var expDateField: EditText
    private lateinit var imageField: ImageView

    private lateinit var categoryValue: String
    private lateinit var spinner: Spinner
    private lateinit var categories: Array<String>

    private lateinit var actionCamera: ImageButton
    private lateinit var soldTextView: TextView
    private lateinit var hiddenTextView: TextView
    private lateinit var ratedTextView:TextView

    private var bool_sold = false
    private var bool_hidden = false
    private var bool_rated=false

    private val REQUEST_IMAGE_CAPTURE = 1

    private val db = Firebase.firestore
    private val storage = Firebase.storage

    private val REQUEST_MAP = 1001

    var uId: MutableList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.item_edit_fragment, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titleField = view.findViewById(R.id.edit_title)
        descField = view.findViewById(R.id.edit_description)
        priceField = view.findViewById(R.id.edit_price)
        locationField = view.findViewById(R.id.edit_location)
        expDateField = view.findViewById(R.id.edit_expiryDate)
        imageField = view.findViewById(R.id.item_image)
        soldTextView = view.findViewById(R.id.item_sold)
        hiddenTextView = view.findViewById(R.id.item_hidden)
        ratedTextView=view.findViewById(R.id.item_rated)

        spinner = view.findViewById(R.id.edit_spinner)
        spinnerInit()

        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]

        expDateField.setOnClickListener{
            val datePicker = DatePickerDialog(this.requireContext(), DatePickerDialog.OnDateSetListener { _, y, m, d ->
                expDateField.setText("$d / $m / $y")
            }, year, month, day)
            datePicker.show()
        }
        
        setDetails()
        itemInstance = Item(
            itemId,
            titleField.text.toString(),
            descField.text.toString(),
            priceField.text.toString(),
            locationField.text.toString(),
            "",
            expDateField.text.toString(),
            "",
            "",
            false,
            bool_sold,
            bool_hidden,
            bool_rated
        )

        actionCamera = view.findViewById(R.id.action_camera)
        actionCamera.setOnClickListener {
            registerForContextMenu(actionCamera)
            activity?.openContextMenu(actionCamera)
        }

        val navController = Navigation.findNavController(this.requireView())
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            navController.popBackStack(R.id.itemListFragment, false)
        }

        val chooseLocation: ImageButton = view.findViewById(R.id.chooseLocation)
        chooseLocation.setOnClickListener {

            val intent = Intent()
            intent.setClass(requireContext(), MapsActivity::class.java)
            startActivityForResult(intent, REQUEST_MAP)
        }

    }

    //------------------------- Edit Menu ----------------------------

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main, menu)
        menu.findItem(R.id.action_edit).isVisible = false

        itemId = arguments?.get("itemId") as String
        db.collection("items")
            .document(itemId)
            .get().addOnSuccessListener { result ->
                val item = result.data
                if (item != null) {
                    menu.findItem(R.id.action_sold).isVisible = bool_sold.not()
                    if (bool_sold) {
                        menu.findItem(R.id.action_show).isVisible = false
                        menu.findItem(R.id.action_hide).isVisible = false
                    } else {
                        menu.findItem(R.id.action_show).isVisible = bool_hidden
                        menu.findItem(R.id.action_hide).isVisible = bool_hidden.not()
                    }

                } else {
                    menu.findItem(R.id.action_sold).isVisible = false
                    menu.findItem(R.id.action_hide).isVisible = false
                    menu.findItem(R.id.action_show).isVisible = false
                }

            }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.action_save -> {
                saveDetailsChanges()
                moveView()
                return true
            }
            R.id.action_sold -> {
                bool_sold = true
                soldTextView.visibility = View.VISIBLE
                val list = mutableListOf<Model>()
                val popupView: View = View.inflate(context, R.layout.popup_window_sold, null)
                val popupWindow = PopupWindow(
                    popupView,
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT
                )

                val popup_listView = popupView.findViewById<ListView>(R.id.popup_listView)

                popupWindow.isFocusable = true

                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)


                db.collection("items").document(itemId).collection("interested_buyers").get()
                    .addOnSuccessListener { result ->
                        if (!result.isEmpty) {
                            for (reference in result) {
                                val fullRef = reference.reference
                                fullRef.get().addOnSuccessListener { res ->
                                    val userId = res.data?.get("id").toString()
                                    uId.add(userId)
                                    db.collection("users").document(userId).get()
                                        .addOnSuccessListener { result ->
                                            val nickName =
                                                result.data?.get("nickname") as CharSequence
                                            list.add(Model(nickName as String))

                                            popup_listView.adapter = context?.let {
                                                PupupUserAdapter(
                                                    it,
                                                    R.layout.popup_item,
                                                    list
                                                )
                                            }
                                        }
                                    popup_listView.setOnItemClickListener { parent: AdapterView<*>,
                                                                            view: View, position: Int,
                                                                            id: Long ->
                                        val bundle = bundleOf("userid" to uId[position])
                                        //adding the item to the bought list of the interested user
                                        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
                                        if (userPref != null) {
                                            val token = userPref.getString("USER_TOKEN", null)
                                            if (token != null) {
                                                //navigate to the chosen users document, and in their bought_items list add the id of the item that they have bought
                                                db.collection("users").document(uId[position]).collection("bought_items").document(itemId).set(hashMapOf("itemId" to itemId))
                                                
                                            }
                                        }
                                        //Dont need to navigate to the profile just dismiss the window after the user is chosen
                                        popupWindow.dismiss()


                                    }
                                }
                            }
                        }
                    }
                saveDetailsChanges()
                //notifyItemSold()
                moveView()
                return true
            }
            R.id.action_hide -> {
                bool_hidden = true
                hiddenTextView.visibility = View.VISIBLE
                saveDetailsChanges()
                //notifyItemHidden()
                moveView()
                return true
            }
            R.id.action_show -> {
                bool_hidden = false
                hiddenTextView.visibility = View.INVISIBLE
                saveDetailsChanges()
                moveView()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun moveView() {
        itemId = arguments?.get("itemId") as String
        val bundle = bundleOf("itemId" to itemId)
        bundle.putInt("previous", 5)
        val navController = Navigation.findNavController(this.requireView())
        navController.navigate(R.id.itemDetailsFragment, bundle)
    }

    //----------------------------- Handle details changes ------------------------------
    private fun setDetails() {
        itemId = arguments?.get("itemId") as String
        db.collection("items")
            .document(itemId)
            .get().addOnSuccessListener { result ->
                val item = result.data
                if (item != null) {
                    titleField.setText(item["TITLE"] as CharSequence)
                    descField.setText(item["DESCRIPTION"] as CharSequence)
                    priceField.setText(item["PRICE"] as CharSequence)
                    locationField.setText(item["LOCATION"] as CharSequence)
                    expDateField.setText(item["EXPIRY_DATE"] as CharSequence)

                    loadImage(itemId)

                    bool_hidden = item.getValue("BOOL_HIDE") as Boolean
                    bool_sold = item.getValue("BOOL_SOLD") as Boolean
                    bool_rated=item.getValue("BOOL_RATED") as Boolean

                    if (bool_sold) soldTextView.visibility = View.VISIBLE
                    if (bool_hidden) hiddenTextView.visibility = View.VISIBLE

                } else {
                    titleField.setText("")
                    descField.setText("")
                    priceField.setText("")
                    locationField.setText("")
                    expDateField.setText("")
                    bool_sold = false
                    bool_hidden = false
                    bool_rated=false
                }
            }

    }

    private fun saveDetailsChanges() {
        itemId = arguments?.get("itemId") as String
        //sharedPref = requireActivity().getSharedPreferences("ItemDetails$itemId", Context.MODE_PRIVATE)

        val title = titleField.text.toString()
        val description = descField.text.toString()
        val price = priceField.text.toString()
        val location = locationField.text.toString()
        val expDate = expDateField.text.toString()
        val imageURL = uploadImage(itemId, imageField.drawable.toBitmap())

        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                val itemInfo =
                    Item(
                        itemId,
                        title,
                        description,
                        price,
                        location,
                        categoryValue,
                        expDate,
                        imageURL,
                        token,
                        false,
                        bool_sold,
                        bool_hidden,
                        bool_rated
                    ).toHashMap()

                val newItemRef = db.collection("items").document(itemId)

                newItemRef.set(itemInfo)
                    .addOnSuccessListener { bindNewItemToUser(itemId, newItemRef) }
                    .addOnFailureListener { e -> Log.w(TAG, "Error registering item", e) }
            } else Log.d(TAG, "Null token")
        } else Log.d(TAG, "Null userPref")

    }

    private fun bindNewItemToUser(itemToken: String, doc: DocumentReference) {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                val userItems = db.collection("users").document(token).collection("items")
                val ref = hashMapOf("path" to doc)
                userItems.document(itemToken).set(ref)
                    .addOnSuccessListener { Log.d(TAG, "Item successfully registered") }
            }
        }
    }

    private fun uploadImage(id:String, bitmap: Bitmap):String {
        val storageRef = storage.reference
        val itemPicRef = storageRef.child("$id.jpg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val uploadTask = itemPicRef.putBytes(baos.toByteArray())
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
        }.addOnSuccessListener { imageField.setImageBitmap(bitmap) }
        return ""
    }

    private fun loadImage(id: String) {
        val itemRef = storage.reference.child("$id.jpg")

        val ONE_MEGABYTE: Long = 1024 * 1024
        itemRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
            val options = BitmapFactory.Options()
            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size, options)
            imageField.setImageBitmap(bitmap)
        }.addOnFailureListener {
            // Handle any errors
        }
    }


    //----------------------------Category's Spinner -----------------------------------

    private fun spinnerInit() {
        categories = resources.getStringArray(R.array.Categories)

        spinner.adapter = context?.let {
            ArrayAdapter(it, R.layout.support_simple_spinner_dropdown_item, categories)
        } as SpinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                categoryValue = parent?.getItemAtPosition(position).toString()
                Toast.makeText(activity, categoryValue, Toast.LENGTH_LONG).show()
            }

        }
    }

    //-------------------------------- Camera and Gallery -------------------------------------
    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.camera_menu, menu)
        menu.setHeaderTitle("Select one action")

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.fromGallery -> {
                return fromGallery()
            }

            R.id.fromcamera -> {
                return fromCamera()
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun fromCamera(): Boolean {
        dispatchTakePictureIntent()
        return true
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    companion object {
        //image pick code
        private const val IMAGE_PICK_CODE = 1000

        //Permission code
        private const val PERMISSION_CODE = 1001
    }

    private fun fromGallery(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED
            ) {
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                //permission already granted
                pickImageFromGallery()
            }
        } else {
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
        return true
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    //permission from popup denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            imageField.setImageBitmap(imageBitmap)
        }
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            data?.data?.let {
                val bitmap =
                    BitmapFactory.decodeStream(requireActivity().contentResolver.openInputStream(it))
                imageField.setImageBitmap(bitmap)
            }
        }
        if(requestCode == REQUEST_MAP && resultCode == Activity.RESULT_OK){
            if (data != null) {
                locationField.setText(data.getStringExtra("streetAddress"))
            }
        }
    }


}
