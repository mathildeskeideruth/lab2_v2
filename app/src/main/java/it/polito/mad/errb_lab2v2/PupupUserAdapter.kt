package it.polito.mad.errb_lab2v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class PupupUserAdapter(var mCtx: Context, var resources: Int, var users: List<Model>) :
    ArrayAdapter<Model>(mCtx, resources, users) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

        val username: TextView = view.findViewById(R.id.pupup_textView)

        var mUser: Model = users[position]
        username.setText(mUser.username)

        return view
    }
}