package it.polito.mad.errb_lab2v2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private val RC_SIGN_IN = 9001

    private lateinit var auth: FirebaseAuth
    private lateinit var user: FirebaseUser
    private lateinit var userToken: String
    private val db = Firebase.firestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // ???????????????????????? bug with the drawer
//        toolbar.setNavigationOnClickListener {
//            this.onBackPressedDispatcher.addCallback(this) {
//                navController.popBackStack(R.id.itemListFragment, false)
//            }
//        }
        // ???????????????????????? bug with the drawer



        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.itemListFragment,
            R.id.showProfileFragment,
            R.id.onSaleListFragment,
            R.id.itemOfInterestFragment,
            R.id.boughtItemsListFragment
        ), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // AUTHENTICATION CODE START ----------------------------------
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        auth = FirebaseAuth.getInstance()
        // AUTHENTICATION CODE END ------------------------------------

        //FIREBASE MESSAGING  -------------------------------------
        //Check google play version
        val googleApi = GoogleApiAvailability.getInstance()
        val res = googleApi.isGooglePlayServicesAvailable(this)
        if (res != ConnectionResult.SUCCESS) {
            googleApi.makeGooglePlayServicesAvailable(this)
        }

        // Notification channel setup
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("ITEM_SOLD_CH", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onStart() {
        super.onStart()
        val user = auth.currentUser
        if (user != null) {
            Log.d("TAG", user.uid)
            FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token!!
                Log.d("TAG", token)
                saveNotificationToken(token)
            })
            updateHeader()
        }
    }

    override fun onResume() {
        super.onResume()
        //Check google play version
        val googleApi = GoogleApiAvailability.getInstance()
        val res = googleApi.isGooglePlayServicesAvailable(this)
        if (res != ConnectionResult.SUCCESS) {
            googleApi.makeGooglePlayServicesAvailable(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d("TAG", "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("TAG", "Google sign in failed", e)
                // ...
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    user = auth.currentUser!!
                    user.getIdToken(true).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val sharedPref = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
                            if (sharedPref != null) {
                                val editor: SharedPreferences.Editor = sharedPref.edit()
                                editor.apply {
                                    putString("USER_TOKEN", user.uid)
                                }.apply()
                            }
                        }
                    }
                    Log.d("TAG", "signInWithCredential:success")
                    updateHeader()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TAG", "signInWithCredential:failure", task.exception)
                }
            }
    }

    fun signIn(view: View) {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    fun signOut() {
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this) {
                // Update your UI here
            }
    }


    private fun updateHeader() {
        val account = GoogleSignIn.getLastSignedInAccount(this)
        val navView = findViewById<NavigationView>(R.id.nav_view)

        if (account != null) {
            navView.removeHeaderView(navView.getHeaderView(0))
            val header = layoutInflater.inflate(R.layout.nav_header_main, navView, false)
            navView.addHeaderView(header)

            val name = navView.getHeaderView(0).findViewById<TextView>(R.id.header_name)
            val email = navView.getHeaderView(0).findViewById<TextView>(R.id.header_email)

            val sharedPref = getSharedPreferences("ProfileInfo", Context.MODE_PRIVATE)
            if (sharedPref != null) {
                name.text = sharedPref.getString("fullname", "Full Name")
                email.text = sharedPref.getString("email", "email@example.com")
            }

        } else {
            Log.d("TAG", "no account found")
        }

    }

    fun saveNotificationToken(token: String) {
        val userPref = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {

            val uid = userPref.getString("USER_TOKEN", null)
            if (uid != null) {
                val editor: SharedPreferences.Editor = userPref.edit()
                editor.apply {
                    putString("USER_MESSAGING_TOKEN", token)
                }.apply()

                val tokens = db.collection("users")
                    .document(uid).collection("notificationTokens")

                val ref = hashMapOf("set" to true)

                tokens.document(token).set(ref)
                    .addOnSuccessListener {
                        // Log and toast
                        Log.d("TAG", "Current token: $token")
                        //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                    }.addOnFailureListener {
                        Log.d("TAG", "fail to save token $token")
                    }
            }
        }
    }
}
