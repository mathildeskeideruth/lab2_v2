package it.polito.mad.errb_lab2v2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val db = Firebase.firestore
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        remoteMessage.data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            handleNow()
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
            showNotification(it, remoteMessage.data)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private fun showNotification(notification: RemoteMessage.Notification, data: Map<String, String>) {
        // Create an explicit intent for an Activity in your app
        val intent = Intent(this, ItemDetailsFragment::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        var builder = NotificationCompat.Builder(this, "ITEM_SOLD_CH")
            .setSmallIcon(R.drawable.shopping_cart_black_18dp)
            .setContentTitle(notification.title)
            .setContentText(notification.body)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(notification.hashCode(), builder.build())
        }

    }


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        val sharedPref = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            val editor: SharedPreferences.Editor = sharedPref.edit()
            editor.apply {
                putString("USER_MESSAGING_TOKEN", token)
            }.apply()
        }
        saveToken(token)

    }

   private fun saveToken(token: String) {
       val userPref = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
       if (userPref != null) {

           val uid = userPref.getString("USER_TOKEN", null)
           if (uid != null) {
               val editor: SharedPreferences.Editor = userPref.edit()
               editor.apply {
                   putString("USER_MESSAGING_TOKEN", token)
               }.apply()

               val tokens = db.collection("users")
                   .document(uid).collection("notificationTokens")

               val ref = hashMapOf("set" to true)

               tokens.document(token).set(ref)
                   .addOnSuccessListener {
                       // Log and toast
                       Log.d("TAG", "Current token: $token")
                       //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                   }.addOnFailureListener {
                       Log.d("TAG", "fail to save token $token")
                   }
           }
       }

       //TODO if a token is changed for security reasons, it will just add a new token in list, not replace old one
   }


    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
        //TODO
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }
}