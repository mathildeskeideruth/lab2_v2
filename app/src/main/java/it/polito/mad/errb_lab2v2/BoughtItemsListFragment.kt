package it.polito.mad.errb_lab2v2

import kotlinx.android.synthetic.main.bought_item_list_fragment.*
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

class BoughtItemsListFragment : Fragment(), OnItemClickListener {
    override fun onItemClick(item: Item) {
    }

    override fun onEditClick(item: Item) {
    }



    private lateinit var items: ArrayList<Item>
    private lateinit var itemAdapter: ItemAdapter

    private val db = Firebase.firestore
    private val storage = Firebase.storage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        items = ArrayList()
        itemAdapter = ItemAdapter(items, this, 4)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bought_item_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bought_rv.adapter = itemAdapter
        bought_rv.layoutManager = LinearLayoutManager(this.context)
        bought_rv.itemAnimator = DefaultItemAnimator()

        if (itemAdapter.itemCount == 0) {
        }
    }

    override fun onResume() {
        super.onResume()
        itemAdapter.items.clear()
        retrieveUserItems()
    }


    private fun retrieveUserItems() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                db.collection("users")
                    .document(token).collection("bought_items").get()
                    .addOnSuccessListener { result ->
                        for (doc in result) { // for each item the user is interested in
                            val itemId = doc.data["itemId"] as String
                            db.collection("items").document(itemId).get()
                                .addOnSuccessListener { res ->
                                    val itemData = res.data
                                    if (itemData != null) {
                                        items.add(
                                            Item(
                                                itemData["ID"].toString(),
                                                itemData["TITLE"].toString(),
                                                itemData["DESCRIPTION"].toString(),
                                                itemData["PRICE"].toString(),
                                                itemData["LOCATION"].toString(),
                                                itemData["CATEGORY"].toString(),
                                                itemData["EXPIRY_DATE"].toString(),
                                                itemData["IMAGE_URL"].toString(),
                                                itemData["SELLER"].toString(),
                                                false,
                                                itemData.getValue("BOOL_SOLD") as Boolean,
                                                itemData.getValue("BOOL_HIDE") as Boolean,
                                                itemData.getValue("BOOL_RATED") as Boolean
                                            )
                                        )
                                    }
                                    itemAdapter.notifyItemInserted(itemAdapter.itemCount - 1)
                                    itemAdapter.updateListItems(items)
                                }
                        }
                    }
            }
        }
    }

}