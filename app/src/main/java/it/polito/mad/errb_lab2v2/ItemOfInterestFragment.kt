package it.polito.mad.errb_lab2v2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.item_of_interest_fragment.*

class ItemOfInterestFragment : Fragment(), OnItemClickListener {
    override fun onItemClick(item: Item) {
    }

    override fun onEditClick(item: Item) {
    }

    private lateinit var items: ArrayList<Item>
    private lateinit var itemAdapter: ItemAdapter

    private val db = Firebase.firestore
    private val storage = Firebase.storage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        items = ArrayList()
        itemAdapter = ItemAdapter(items, this, 3)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_of_interest_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        interest_rv.adapter = itemAdapter
        interest_rv.layoutManager = LinearLayoutManager(this.context)
        interest_rv.itemAnimator = DefaultItemAnimator()

        if (itemAdapter.itemCount == 0) {
        }
    }

    override fun onResume() {
        super.onResume()
        itemAdapter.items.clear()
        retrieveUserItems()
    }


    private fun retrieveUserItems() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                db.collection("users")
                    .document(token).collection("items_of_interest").get()
                    .addOnSuccessListener { result ->
                        for (doc in result) { // for each item the user is interested in
                            val itemId = doc.data["itemId"] as String
                            db.collection("items").document(itemId).get()
                                .addOnSuccessListener { res ->
                                val itemData = res.data
                                if (itemData != null) {
                                    val sold = itemData.getValue("BOOL_SOLD") as Boolean
                                    val hidden = itemData.getValue("BOOL_HIDE") as Boolean
                                    if(sold.not() && hidden.not()) {
                                        items.add(
                                            Item(
                                                itemData["ID"].toString(),
                                                itemData["TITLE"].toString(),
                                                itemData["DESCRIPTION"].toString(),
                                                itemData["PRICE"].toString(),
                                                itemData["LOCATION"].toString(),
                                                itemData["CATEGORY"].toString(),
                                                itemData["EXPIRY_DATE"].toString(),
                                                itemData["IMAGE_URL"].toString(),
                                                itemData["SELLER"].toString(),
                                                false,
                                                sold,
                                                hidden,
                                                itemData.getValue("BOOL_RATED") as Boolean

                                            )
                                        )
                                    }
                                }
                                itemAdapter.notifyItemInserted(itemAdapter.itemCount - 1)
                                itemAdapter.updateListItems(items)
                            }
                        }
                    }
            }
        }
    }

}