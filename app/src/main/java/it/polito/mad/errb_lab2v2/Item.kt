package it.polito.mad.errb_lab2v2

import java.io.Serializable

data class Item(var id : String,
                var title: String,
                var description: String,
                var price: String,
                var location:String,
                var category: String,
                var expDate: String,
                var imageURL: String,
                var seller: String,
                var empty: Boolean,
                var isSold: Boolean,
                var isHidden: Boolean,
                var isRated: Boolean
) : Serializable {
    fun toHashMap(): HashMap<String, Any> {
        return hashMapOf(
            "ID" to id,
            "TITLE" to title,
            "DESCRIPTION" to description,
            "PRICE" to price,
            "LOCATION" to location,
            "EXPIRY_DATE" to expDate,
            "SELLER" to seller,
            "CATEGORY" to category,
            "IMAGE_URL" to imageURL,
            "BOOL_SOLD" to isSold,
            "BOOL_HIDE" to isHidden,
            "BOOL_RATED" to isRated

        )
    }
}
