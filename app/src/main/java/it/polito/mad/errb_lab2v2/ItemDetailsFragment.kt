package it.polito.mad.errb_lab2v2

import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.item_details_fragment.*

class ItemDetailsFragment : Fragment(), OnMapReadyCallback {


    private val db = Firebase.firestore
    private val storage = Firebase.storage

    private var itemId: String = ""

    private lateinit var titleField: TextView
    private lateinit var descField: TextView
    private lateinit var priceField: TextView
    private lateinit var categoryField: TextView
    private lateinit var locationField: TextView
    private lateinit var expDateField: TextView
    private lateinit var imageField: ImageView

    private lateinit var soldTextView: TextView
    private lateinit var hiddenTextView: TextView

    private lateinit var mMapView: MapView

    private lateinit var mGoogleMap: GoogleMap

    private lateinit var ratedTextView: TextView
    var uId: MutableList<String> = arrayListOf()


    private var bool_sold = false
    private var bool_hidden = false
    private var bool_rated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.item_details_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Back button
        val navController = Navigation.findNavController(this.requireView())
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            navController.popBackStack(R.id.itemListFragment, false)
        }

        titleField = view.findViewById(R.id.title)
        descField = view.findViewById(R.id.description)
        priceField = view.findViewById(R.id.price)
        categoryField = view.findViewById(R.id.category)
        locationField = view.findViewById(R.id.location)
        expDateField = view.findViewById(R.id.expiryDate)
        imageField = view.findViewById(R.id.item_image)
        soldTextView = view.findViewById(R.id.item_sold)
        hiddenTextView = view.findViewById(R.id.item_hidden)
        mMapView = view.findViewById(R.id.map)
        ratedTextView = view.findViewById(R.id.item_rated)

        setDetails()

        val previous = arguments?.getInt("previous")
        if (previous != 2) {
            floatingActionButton.hide()
        }
        if (previous != 1) {
            button_buyers.isVisible = false
        }
        if (previous == 10) {//The previous list was the bought_items_list and the user pressed the rate seller button
            showPopupWindow()
        }

        mMapView.onCreate(null)
        mMapView.onResume()
        mMapView.getMapAsync(this)

        setInterestedBuyers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val previous = arguments?.getInt("previous")
        if (previous == 1 || previous == 5) {
            inflater.inflate(R.menu.main, menu)
            menu.apply {
                //findItem(R.id.editProfileFragment).isVisible = (previous != 2)
                findItem(R.id.action_edit).isVisible = true
                findItem(R.id.action_sold).isVisible = false
                findItem(R.id.action_hide).isVisible = false
                findItem(R.id.action_save).isVisible = false
                findItem(R.id.action_show).isVisible = false
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.action_edit -> {
                saveDetailsChanges()
                itemId = arguments?.get("itemId") as String
                val bundle = bundleOf("itemId" to itemId)
                val navController = Navigation.findNavController(this.requireView())
                navController.navigate(R.id.itemEditFragment, bundle)
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

    //----------------------------------- Save / Set details --------------------------------------
    private fun setDetails() {

        itemId = arguments?.get("itemId") as String
        val itemRef = db.collection("items").document(itemId)
        itemRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed", e)
                return@addSnapshotListener
            }
            if (snapshot != null && snapshot.exists()) {
                val data = snapshot.data
                if (data != null) {
                    itemRef.set(data)
                    val price = data["PRICE"] as CharSequence?
                    titleField.text = data["TITLE"] as CharSequence?
                    descField.text = data["DESCRIPTION"] as CharSequence?
                    priceField.text = price.toString() + "€"
                    locationField.text = data["LOCATION"] as CharSequence?
                    expDateField.text = data["EXPIRY_DATE"] as CharSequence?
                    categoryField.text = data["CATEGORY"] as CharSequence?

                    loadImage(itemId)

                    bool_hidden = data.getValue("BOOL_HIDE") as Boolean
                    bool_sold = data.getValue("BOOL_SOLD") as Boolean
                    bool_rated = data.getValue("BOOL_RATED") as Boolean

                    if (bool_sold)
                        soldTextView.visibility = View.VISIBLE


                    if (bool_hidden) hiddenTextView.visibility = View.VISIBLE
                }
                Log.d(TAG, "Current data : $data")
            } else {
                Log.d(TAG, "Current data : null")
            }
        }

    }

    private fun loadImage(id: String) {
        val itemRef = storage.reference.child("$id.jpg")

        val ONE_MEGABYTE: Long = 1024 * 1024
        itemRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
            val options = BitmapFactory.Options()
            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size, options)
            imageField.setImageBitmap(bitmap)
        }.addOnFailureListener {
            // Handle any errors
        }
    }

    private fun saveDetailsChanges() {
        itemId = arguments?.get("itemId") as String

        val title = titleField.text.toString()
        val description = descField.text.toString()
        val price = priceField.text.toString()
        val location = locationField.text.toString()
        val expDate = expDateField.text.toString()
        val category = categoryField.text.toString()

        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                val itemInfo =
                    Item(
                        itemId,
                        title,
                        description,
                        price,
                        location,
                        category,
                        expDate,
                        "",
                        token,
                        false,
                        bool_sold,
                        bool_hidden,
                        bool_rated
                    ).toHashMap()

                val newItemRef = db.collection("items").document(itemId)

                newItemRef.set(itemInfo)
                    .addOnSuccessListener {
                        Log.d(TAG, "Current data : $itemInfo")
                    }
                    .addOnFailureListener { e -> Log.w(TAG, "Error registering item", e) }
            } else Log.d(TAG, "Null token")
        } else Log.d(TAG, "Null userPref")

    }

    //------------------------------- Interested buyers --------------------------------------------
    private fun setInterestedBuyers() {
        var fagClicked = false
        itemId = arguments?.get("itemId") as String
        val user = FirebaseAuth.getInstance().currentUser
        val currentUserId = user?.uid
        val list = mutableListOf<Model>()

        db.collection("items").document(itemId).get().addOnSuccessListener { result ->
            val itemData = result.data
            val seller = itemData?.get("SELLER")
            if (seller == currentUserId) {
                floatingActionButton.hide()
            }

        }

        db.collection("items").document(itemId).collection("interested_buyers").get()
            .addOnSuccessListener { result ->
                //For each interested buyer
                if (!result.isEmpty) {
                    for (reference in result) {
                        val fullRef = reference.reference
                        fullRef.get().addOnSuccessListener { res ->
                            val itemData = res.data
                            val id = itemData?.get("id")
                            if (id == currentUserId) {
                                floatingActionButton.setImageResource(android.R.drawable.star_big_on)
                                fagClicked = true
                            }
                        }
                    }
                }
            }


        floatingActionButton.setOnClickListener {
            val userAuthentication = FirebaseAuth.getInstance().currentUser
            val currentUserId = userAuthentication!!.uid

            if (fagClicked) {
                db.collection("items").document(itemId).collection("interested_buyers").get()
                    .addOnSuccessListener { result ->
                        for (ref in result) {
                            val id = ref.data.get("id")
                            if (id == currentUserId) {
                                ref.reference.delete()
                                floatingActionButton.setImageResource(android.R.drawable.star_big_off)
                                fagClicked = false
                                Log.d("DELETED", "DELETED")

                                notifyLossInterest()
                            }

                        }
                    }


            } else {
                floatingActionButton.setImageResource(android.R.drawable.star_big_on)

                val userPref =
                    requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
                if (userPref != null) {
                    val token = userPref.getString("USER_TOKEN", null)
                    if (token != null) {
                        db.collection("users").document(token).get()
                            .addOnSuccessListener { user ->
                                db.collection("items").document(itemId)
                                    .collection("interested_buyers")
                                    .document(token).set(user)

                                db.collection("users").document(token)
                                    .collection("items_of_interest")
                                    .document(itemId).set(hashMapOf("itemId" to itemId))
                            }
                    }
                }
                fagClicked = true
            }
            if (fagClicked) notifyInterest()
        }

        button_buyers.setOnClickListener {

            val popupView: View = View.inflate(context, R.layout.popup_window, null)
            val popupWindow = PopupWindow(
                popupView,
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT
            )

            val popup_listView = popupView.findViewById<ListView>(R.id.popup_listView)

            popupWindow.isFocusable = true

            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)


            db.collection("items").document(itemId).collection("interested_buyers").get()
                .addOnSuccessListener { result ->
                    if (!result.isEmpty) {
                        for (reference in result) {
                            val fullRef = reference.reference
                            fullRef.get().addOnSuccessListener { res ->
                                val userId = res.data?.get("id").toString()
                                uId.add(userId)
                                db.collection("users").document(userId).get()
                                    .addOnSuccessListener { result ->
                                        val nickName =
                                            result.data?.get("nickname") as CharSequence
                                        list.add(Model(nickName as String))

                                        popup_listView.adapter = context?.let {
                                            PupupUserAdapter(
                                                it,
                                                R.layout.popup_item,
                                                list
                                            )
                                        }
                                    }

                                popup_listView.setOnItemClickListener { parent: AdapterView<*>,
                                                                        view: View, position: Int,
                                                                        id: Long ->
                                    val bundle = bundleOf("userid" to uId[position])
                                    val navController =
                                        Navigation.findNavController(this.requireView())
                                    navController.navigate(R.id.showProfileFragment, bundle)
                                    popupWindow.dismiss()

                                }
                            }
                        }
                    }
                }
        }

    }

    private fun notifyInterest() {
        itemId = arguments?.get("itemId") as String

        FirebaseMessaging.getInstance().subscribeToTopic(itemId)
            .addOnCompleteListener { task ->
                var msg = getString(R.string.msg_subscribed)
                if (!task.isSuccessful) {
                    msg = getString(R.string.msg_subscribe_failed)
                }
                Log.d(TAG, msg)
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            }
    }

    private fun notifyLossInterest() {
        itemId = arguments?.get("itemId") as String

        FirebaseMessaging.getInstance().unsubscribeFromTopic(itemId)
            .addOnCompleteListener { task ->
                var msg = getString(R.string.msg_unsubscribed)
                if (!task.isSuccessful) {
                    msg = getString(R.string.msg_unsubscribe_failed)
                }
                Log.d(TAG, msg)
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            }
    }

    //------------------------------- Setup MapView --------------------------------------------


    override fun onMapReady(p0: GoogleMap?) {

        MapsInitializer.initialize(context)
        if (p0 != null) {
            val geocoder = Geocoder(context)
            val address = geocoder.getFromLocationName(locationField.text.toString(), 1)
            if (address == null) {
                Toast.makeText(context, "Location is not set", Toast.LENGTH_LONG).show()
            } else {
                val addr = if (address.size==0) 0 else address.size-1
                val location = address[addr]
                val lat = location.latitude
                val lng = location.longitude
                mGoogleMap = p0
                p0.mapType = GoogleMap.MAP_TYPE_NORMAL
                p0.addMarker(
                    MarkerOptions().position(LatLng(lat, lng)).title(location.getAddressLine(0))
                )

                val loc = CameraPosition.builder()
                    .target(LatLng(lat, lng))
                    .zoom(16F)
                    .bearing(0F)
                    .tilt(45F)
                    .build()

                p0.moveCamera(CameraUpdateFactory.newCameraPosition(loc))
            }

        }
    }

    private fun showPopupWindow() {
        //if this is shown then setting is rated to true
        //PREVIOUS QUERY that maybe made the database crash
        //db.collection("items").document(itemId).update("BOOL_RATED",true)

        val popupView: View = View.inflate(context, R.layout.popup_window_rate, null)
        val popupWindow = PopupWindow(
            popupView,
            WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT
        )

        popupWindow.isFocusable = true

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        val rb = popupView.findViewById(R.id.rBar) as RatingBar
        val rating = rb.rating
        Log.d("RATING", rating.toString())

        //Get seller id
        itemId = arguments?.get("itemId") as String
        db.collection("items").document(itemId).get()
            .addOnSuccessListener { result ->
                val itemData = result.data
                val seller = itemData?.get("SELLER").toString()

                //Get seller ratings
                db.collection("users").document(seller).get()
                    .addOnSuccessListener { res ->
                        val userdata = res.data
                        var numRating = userdata?.get("numrating") as Long
                        Log.d("numrating", numRating.toString())

                        val currentRating = userdata["rate"] as Number
                        var tempRating =
                            0.0 //needed this to collect first time when field value is 0 before first rating
                        if (currentRating != 0) {
                            tempRating += currentRating.toDouble()
                        }

                        rb.onRatingBarChangeListener =
                            OnRatingBarChangeListener { _, newRating, _ ->
                                numRating += 1

                                val newAvg =
                                    ((tempRating * (numRating - 1)) + newRating) / numRating
                                Log.d("UPDATED RATING", newAvg.toString())

                                val updates = hashMapOf<String, Any>(
                                    "numrating" to numRating,
                                    "rate" to newAvg
                                )

                                db.collection("users").document(seller)
                                    .update(updates)
                                    .addOnSuccessListener {
                                        Log.d(
                                            TAG,
                                            "Rating successfully updated"
                                        )
                                        bool_rated = true
                                        saveDetailsChanges()
                                    }

                            }
                    }


            }

    }

}



