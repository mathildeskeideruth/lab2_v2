package it.polito.mad.errb_lab2v2

import android.Manifest
import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.io.*

class EditProfileFragment : Fragment() {
    companion object {
        //image pick code
        private const val IMAGE_PICK_CODE = 1000

        //Permission code
        private const val PERMISSION_CODE = 1002

        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_MAP = 1002
    }


    private val db = Firebase.firestore
    private val storage = Firebase.storage
    private lateinit var uid: String

    private lateinit var  nameField : EditText
    private lateinit var  nicknameField : EditText
    private lateinit var  emailField : EditText
    private lateinit var  locationField : EditText
    private lateinit var rateField:TextView
    private lateinit var numrateField:TextView
    private lateinit var imageField : ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.edit_profile_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameField = view.findViewById(R.id.name_edit)
        nicknameField = view.findViewById(R.id.nickname_edit)
        emailField = view.findViewById(R.id.email_edit)
        locationField = view.findViewById(R.id.location_edit)
        imageField = view.findViewById(R.id.UserProfilePicture)

        loadPreferences()

        val button: ImageButton =  view.findViewById(R.id.editPictureOverlay)
        button.setOnClickListener { v: View? -> showCameraPopUp(v) }
        val chooseLoc: ImageButton = view.findViewById(R.id.chooseLoc)
              chooseLoc.setOnClickListener {

                       val intent: Intent = Intent()
                       intent.setClass(requireContext(), MapsActivity::class.java)
                        startActivityForResult(intent, REQUEST_MAP)
                    }
    }

    // ------------------ Save menu ------------------

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.save_edits_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_edits -> {
                saveChanges()
                val navController = Navigation.findNavController(this.requireView())
                navController.navigate(R.id.showProfileFragment)
                return true
            }
            else -> false
        }
    }

    private fun loadPreferences() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                uid = token
                val userInfo = db.collection("users").document(token)
                userInfo.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            setTextFields(document)
                            Log.d(TAG, "UserInfo data: ${document.data}")
                        } else Log.d(TAG, "No such document")
                    }.addOnFailureListener{ e -> Log.d(TAG, "get failed with ", e)
                    }
            }
        }
    }

    private fun setTextFields(document: DocumentSnapshot) {
        var name = document.get("fullName").toString()
        var nickname = document.get("nickname").toString()
        var email = document.get("email").toString()
        var location = document.get("location").toString()
        var rate = document.get("rate").toString()
        var numrate=document.get("numrating").toString()

        if (name == "null") name = "Full Name"
        if (nickname == "null") nickname = "Nickname"
        if (email == "null") email = "email@example.com"
        if (location == "null") location = "Location"
        if (rate == "null") rate = "0"
        if(numrate=="null") numrate="0"

        nameField.setText(name)
        nicknameField.setText(nickname)
        emailField.setText(email)
        locationField.setText(location)

        loadImage()
    }

    private fun saveChanges() {
        val name = nameField.text.toString()
        val nickname = nicknameField.text.toString()
        val email = emailField.text.toString()
        val location = locationField.text.toString()

        val userInfo = hashMapOf(
            "fullName" to name,
            "nickname" to nickname,
            "email" to email,
            "location" to location
        )

        val editor: SharedPreferences.Editor = requireActivity()
            .getSharedPreferences("ProfileInfo", Context.MODE_PRIVATE)
            .edit()
        editor.apply {
            putString("fullname", name)
            putString("nickname", nickname)
            putString("email", email)
            putString("locatton", location)
        }.apply()

        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                db.collection("users").document(token).set(userInfo)
                    .addOnSuccessListener { Log.d(TAG, "User successfully registered!") }
                    .addOnFailureListener { e -> Log.w(TAG, "Error registering user", e) }
            } else Log.d(TAG, "Null token")
        } else Log.d(TAG, "Null userPref")

        saveProfilePicture(imageField.drawable.toBitmap())
        updateHeader(requireActivity().findViewById(R.id.nav_view))
        uploadImage()
    }

    private fun updateHeader(navView: NavigationView) {
        loadHeaderImageFromStorage(navView)

        val name = navView.getHeaderView(0).findViewById<TextView>(R.id.header_name)
        val email = navView.getHeaderView(0).findViewById<TextView>(R.id.header_email)

        val sharedPref = activity?.getSharedPreferences("ProfileInfo", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            name.text = sharedPref.getString("fullname", "Full Name")
            email.text = sharedPref.getString("email", "email@example.com")
        }
    }

    private fun loadHeaderImageFromStorage(navView: NavigationView) {
        try {
            val cw = ContextWrapper(requireActivity())
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            val f = File(directory, "profile.jpg")
            val b = BitmapFactory.decodeStream(FileInputStream(f))

            navView.getHeaderView(0)
                .findViewById<ImageView>(R.id.headerProfilePicture)?.setImageBitmap(b)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    // ------------------ Camera menu ------------------
    private fun showCameraPopUp(v: View?) {
        val popup = PopupMenu(requireActivity(), v)
        popup.menuInflater.inflate(R.menu.picture_edit_menu, popup.menu)

        popup.setOnMenuItemClickListener { when(it.itemId) {
            R.id.gallery -> editFromGallery()
            R.id.camera -> editFromCamera()
            else -> false
        } }
        popup.show()
    }

    private fun editFromCamera(): Boolean {
        Log.d("EditProfileActivity", "camera")
        dispatchTakePictureIntent()
        return true
    }

    private fun editFromGallery(): Boolean {
        Log.d("EditProfileActivity", "picture")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery()
            }
        }
        else{
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
        return true
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val storageRef = storage.reference
        if(requestCode == REQUEST_MAP && resultCode == Activity.RESULT_OK){

            if (data != null) {
                locationField.setText(data.getStringExtra("streetAddress"))
            }

        }

        // Take a picture
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val bitmap = data?.extras?.get("data") as Bitmap
            imageField.setImageBitmap(bitmap)
        }
        // Pick from gallery
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            data?.data?.let {
                val bitmap = BitmapFactory.decodeStream(requireActivity().contentResolver?.openInputStream(it))
                imageField.setImageBitmap(bitmap)
            }
        }
    }

    private fun uploadImage(bitmap: Bitmap) {
        val storageRef = storage.reference
        val userPicRef = storageRef.child("$uid.jpg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val uploadTask = userPicRef.putBytes(baos.toByteArray())
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
        }.addOnSuccessListener { imageField.setImageBitmap(bitmap) }
    }

    private fun saveProfilePicture(image: Bitmap?) {
        if (image != null) uploadImage(image)

        val cw = ContextWrapper(requireActivity())
        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
        val mypath = File(directory, "profile.jpg")

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            image?.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun uploadImage() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                val storageRef = storage.reference
                val itemPicRef = storageRef.child("$token.jpg")
                val bitmap = imageField.drawable.toBitmap()

                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val uploadTask = itemPicRef.putBytes(baos.toByteArray())
                uploadTask.addOnFailureListener {
                    // Handle unsuccessful uploads
                }.addOnSuccessListener { imageField.setImageBitmap(bitmap) }
            } else Log.d(TAG, "Null token")
        } else Log.d(TAG, "Null userPref")
    }

    private fun loadImage() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                val itemRef = storage.reference.child("$token.jpg")

                val ONE_MEGABYTE: Long = 1024 * 1024
                itemRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size, options)
                    imageField.setImageBitmap(bitmap)
                }.addOnFailureListener {
                    // Handle any errors
                }
            }
        }
    }
}