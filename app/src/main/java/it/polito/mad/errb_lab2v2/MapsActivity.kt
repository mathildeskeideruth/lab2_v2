package it.polito.mad.errb_lab2v2

import android.app.Activity
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Button
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapLongClickListener,
    GoogleMap.OnMarkerDragListener, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var placesClient: PlacesClient
    private lateinit var window: PopupWindow

    private var placeFields = listOf(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        initPlaces()
        window = PopupWindow(this)

    }

    override fun onPause() {
        super.onPause()
        window.dismiss()
    }

    private fun initPlaces() {
        Places.initialize(this, getString(R.string.places_api))
        placesClient = Places.createClient(this)

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapLongClickListener(this)
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMarkerDragListener(this)
        val geocoder = Geocoder(this)

        val request = FindCurrentPlaceRequest.builder(placeFields).build()
        val placeResponse = placesClient.findCurrentPlace(request)

        placeResponse.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val response = task.result

                response!!.placeLikelihoods.sortWith(Comparator { placeChildHood, t1 ->
                    placeChildHood.likelihood.compareTo(t1.likelihood)
                })
                response.placeLikelihoods.reverse()
                val address = response.placeLikelihoods[0].place.address

                val adds = geocoder.getFromLocationName(address, 1)
                val add = adds.get(0)
                val curLoc = LatLng(add.latitude, add.longitude)
                val markerOptions: MarkerOptions = MarkerOptions()
                    .position(curLoc)
                    .title(address)
                    .draggable(true)

                mMap.addMarker(markerOptions)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curLoc, 16F))
            }
        }

    }


    override fun onMapLongClick(p0: LatLng?) {
        val geocoder = Geocoder(this)
        val addresses: List<Address> = geocoder.getFromLocation(p0!!.latitude, p0.longitude, 1)
        if (addresses.isNotEmpty()) {
            val address = addresses[addresses.size - 1]
            val streetAddress = address.getAddressLine(0)
            mMap.clear()
            mMap.addMarker(
                MarkerOptions()
                    .position(p0)
                    .title(streetAddress)
                    .draggable(true)
            )
        }
    }

    override fun onMarkerDragEnd(p0: Marker?) {
        val latlng = p0!!.position
        val geocoder = Geocoder(this)
        val addresses: List<Address> =
            geocoder.getFromLocation(latlng.latitude, latlng.longitude, 1)
        if (addresses.isNotEmpty()) {
            val address = addresses[addresses.size - 1]
            val streetAddress = address.getAddressLine(0)
            p0.title = streetAddress
        }

    }


    override fun onMarkerDragStart(p0: Marker?) {
        Log.d("onMarkerDragStartTag", "onMarkerDragStart" + p0.toString())
    }


    override fun onMarkerDrag(p0: Marker?) {
        Log.d("OnMarkerDragTag", "onMarkerDrag" + p0.toString())
    }


    override fun onMarkerClick(p0: Marker?): Boolean {
        val view = layoutInflater.inflate(R.layout.map_popup, null)
        window.contentView = view
        window.showAtLocation(view, Gravity.CENTER, 0, 0 )
        val buttonYes = view.findViewById<Button>(R.id.buttonYes)
        val buttonNo = view.findViewById<Button>(R.id.buttonNo)
        buttonYes.setOnClickListener {
            val geocoder = Geocoder(this)
            val addresses: List<Address> =
                geocoder.getFromLocation(p0!!.position.latitude, p0.position.longitude, 1)
            if (addresses.size > 0) {
                val address = addresses.get(addresses.size - 1)
                val streetAddress = address.getAddressLine(addresses.size - 1)
                val returnIntent = intent
                returnIntent.putExtra("streetAddress", streetAddress)
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }
        }
        buttonNo.setOnClickListener {
            window.dismiss()
        }

        window.showAsDropDown(view)
        return true
    }
}
