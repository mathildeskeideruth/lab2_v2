package it.polito.mad.errb_lab2v2

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.on_sale_list_fragment.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class OnSaleListFragment : Fragment(), OnItemClickListener {

    private lateinit var items: ArrayList<Item>
    private lateinit var itemAdapter: ItemAdapter
    var displayItems: MutableList<Item> = arrayListOf()


    private val db = Firebase.firestore
    private val storage = Firebase.storage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        items = ArrayList()
        displayItems.addAll(items)
        itemAdapter = ItemAdapter(displayItems, this, 2)
        retrieveItems()
    }

    private fun retrieveItems() {
        db.collection("items")
            .get()
            .addOnSuccessListener { result ->
                for (doc in result) {
                    val itemData = doc.data
                    val hide = itemData.getValue("BOOL_HIDE") as Boolean
                    val sold = itemData.getValue("BOOL_SOLD") as Boolean
                    val rated = itemData.getValue("BOOL_RATED") as Boolean

                    if (hide.not() && sold.not()) {
                        items.add(
                            Item(
                                itemData.get("ID").toString(),
                                itemData.get("TITLE").toString(),
                                itemData.get("DESCRIPTION").toString(),
                                itemData.get("PRICE").toString(),
                                itemData.get("LOCATION").toString(),
                                itemData.get("CATEGORY").toString(),
                                itemData.get("EXPIRY_DATE").toString(),
                                itemData.get("IMAGE_URL").toString(),
                                itemData.get("SELLER").toString(), false,
                                hide,
                                sold,
                                rated
                            )
                        )

                        itemAdapter.notifyItemInserted(itemAdapter.itemCount - 1)
                        itemAdapter.updateListItems(items)
                    }
                }
                displayItems.addAll(itemAdapter.items)
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.on_sale_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rw_sale_items.adapter = itemAdapter
        rw_sale_items.layoutManager = LinearLayoutManager(this.context)
        rw_sale_items.itemAnimator = DefaultItemAnimator()

        if (displayItems.isEmpty()) {
        }

    }


    // ---------------------------------- SEARCHVIEW ----------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.on_sale_item_search_menu, menu)
        val menuItem = menu.findItem(R.id.search)

        if (menuItem != null) {
            val searchView = menuItem.actionView as SearchView

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText!!.isNotEmpty()) {
                        displayItems.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        items.forEach {
                            if (it.title.toLowerCase(Locale.getDefault()).contains(search)) {
                                displayItems.add(it)
                            }
                            if (it.category.toLowerCase(Locale.getDefault()).contains(search)) {
                                displayItems.add(it)
                            }
                            if (it.price.toLowerCase(Locale.getDefault()).contains(search)) {
                                displayItems.add(it)
                            }
                            if (it.location.toLowerCase(Locale.getDefault()).contains(search)) {
                                displayItems.add(it)
                            }
                        }
                        itemAdapter.notifyDataSetChanged()
                        itemAdapter.updateListItems(displayItems as ArrayList<Item>)
                    } else {
                        displayItems.clear()
                        displayItems.addAll(items)
                        itemAdapter.notifyDataSetChanged()
                    }

                    return true
                }

            })
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    // ---------------------------------- SEARCHVIEW END----------------------------------


    private fun saveDetailsChanges(item: Item) {
        val itemId = item.id
        val itemSnapshot =
            db.collection("items").document(itemId).get().addOnSuccessListener { res ->
                val newItem = res.data
                if (newItem != null) {
                    item.apply {
                        title = newItem["TITLE"].toString()
                        description = newItem["DESCRIPTION"].toString()
                        price = newItem["PRICE"].toString()
                        location = newItem["LOCATION"].toString()
                        expDate = newItem["EXPIRY_DATE"].toString()
                        category = newItem["CATEGORY"].toString()
                        //TODO image
                        isSold = newItem.getValue("BOOL_SOLD") as Boolean
                        isHidden = newItem.getValue("BOOL_HIDE") as Boolean
                        isRated = newItem.getValue("BOOL_RATED") as Boolean

                    }

                }
            }

        itemAdapter.updateListItems(items)
    }


    private fun saveItemPicture(image: Bitmap?) {
        val cw = ContextWrapper(requireActivity())
        val directory: File = cw.getDir("itemImage", Context.MODE_PRIVATE)
        val myPath = File(directory, "itemImage.jpg")

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(myPath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            image?.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }


    override fun onItemClick(item: Item) {
        saveDetailsChanges(item)
    }

    override fun onEditClick(item: Item) {
        TODO("Not yet implemented")
    }

}