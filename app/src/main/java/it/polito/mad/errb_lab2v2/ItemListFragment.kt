package it.polito.mad.errb_lab2v2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.item_list_fragment.*


class ItemListFragment : Fragment(), OnItemClickListener {

    private lateinit var items: ArrayList<Item>
    private lateinit var itemAdapter: ItemAdapter

    private val db = Firebase.firestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        items = ArrayList()
        itemAdapter = ItemAdapter(items, this, 1)
    }

    private fun retrieveUserItems() {
        val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        if (userPref != null) {
            val token = userPref.getString("USER_TOKEN", null)
            if (token != null) {
                db.collection("users")
                    .document(token).collection("items").get()
                    .addOnSuccessListener { result ->
                        for (doc in result) { // for each item sold by the user
                            val ref = doc.data["path"] as DocumentReference
                            ref.get().addOnSuccessListener { res ->
                                val itemData = res.data
                                if (itemData != null) {
                                    items.add(
                                        Item(
                                            itemData.get("ID").toString(),
                                            itemData.get("TITLE").toString(),
                                            itemData.get("DESCRIPTION").toString(),
                                            itemData.get("PRICE").toString(),
                                            itemData.get("LOCATION").toString(),
                                            itemData.get("CATEGORY").toString(),
                                            itemData.get("EXPIRY_DATE").toString(),
                                            itemData.get("IMAGE_URL").toString(),
                                            itemData.get("SELLER").toString(),
                                            false,
                                            itemData.getValue("BOOL_SOLD") as Boolean,
                                            itemData.getValue("BOOL_HIDE") as Boolean,
                                            itemData.getValue("BOOL_RATED") as Boolean

                                        )
                                    )
                                }

                                itemAdapter.notifyItemInserted(itemAdapter.itemCount - 1)
                                itemAdapter.updateListItems(items)


                            }
                        }
                    }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_list_fragment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv.adapter = itemAdapter
        rv.layoutManager = LinearLayoutManager(this.context)
        rv.itemAnimator = DefaultItemAnimator()

        if (itemAdapter.itemCount == 0) {
        }
        fab.setOnClickListener {
            val id = createItem()
            val bundle = bundleOf("itemId" to id)
            val navController = Navigation.findNavController(this.requireView())
            navController.navigate(R.id.itemEditFragment, bundle)
        }
    }

    override fun onResume() {
        super.onResume()
        itemAdapter.items.clear()
        retrieveUserItems()
    }

    private fun createItem(): String {
        val newItemRef = db.collection("items").document()

        val path = newItemRef.path.split('/')
        val itemToken = path[path.size - 1]


        val newItem = emptyItem(itemToken)
        items.add(newItem)
        itemAdapter.updateListItems(items)
        itemAdapter.notifyItemInserted(itemAdapter.itemCount - 1)
        return itemToken
    }

    private fun emptyItem(id: String): Item {
        return Item(id, "", "", "", "", "", "", "", "", true, false, false, false)

    }

    private fun saveDetailsChanges(item: Item) {
        val itemId = item.id
        db.collection("items").document(itemId).get().addOnSuccessListener { res ->
            val newItem = res.data
            if (newItem != null) {
                item.apply {
                    title = newItem["TITLE"].toString()
                    description = newItem["DESCRIPTION"].toString()
                    price = newItem["PRICE"].toString()
                    location = newItem["LOCATION"].toString()
                    expDate = newItem["EXPIRY_DATE"].toString()
                    category = newItem["CATEGORY"].toString()
                    isSold = newItem.getValue("BOOL_SOLD") as Boolean
                    isHidden = newItem.getValue("BOOL_HIDE") as Boolean
                    isRated = newItem.getValue("BOOL_RATED") as Boolean
                }
            }
        }

        itemAdapter.updateListItems(items)
    }

    override fun onItemClick(item: Item) {
        saveDetailsChanges(item)
    }

    override fun onEditClick(item: Item) {
        item.empty = false
        saveDetailsChanges(item)
    }
}


