package it.polito.mad.errb_lab2v2

import android.content.ContentValues
import android.content.Context
import android.content.ContextWrapper
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException

class ShowProfileFragment : Fragment() {
    private lateinit var nameField: TextView
    private lateinit var nicknameField: TextView
    private lateinit var emailField: TextView
    private lateinit var locationField: TextView
    private lateinit var imageField: ImageView
    private lateinit var rateField:TextView
    private lateinit var numrateField:TextView

    private var userId: String? = ""


    private val db = Firebase.firestore
    private val storage = Firebase.storage
    private lateinit var uid: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userId = arguments?.get("userid") as? String
        if (userId.isNullOrEmpty())
            setHasOptionsMenu(true)
        return inflater.inflate(R.layout.show_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameField = view.findViewById(R.id.user_full_name)
        nicknameField = view.findViewById(R.id.user_nickname)
        emailField = view.findViewById(R.id.user_email)
        locationField = view.findViewById(R.id.user_location)
        imageField = view.findViewById(R.id.UserProfilePicture)
        rateField=view.findViewById(R.id.user_rating)
        numrateField=view.findViewById(R.id.user_numrating)


        loadPreferences()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

    private fun loadPreferences() {
        userId = arguments?.get("userid") as? String
        if (userId.isNullOrEmpty()) {
            val userPref = requireActivity().getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
            if (userPref != null) {
                val token = userPref.getString("USER_TOKEN", null)
                if (token != null) {
                    uid = token
                    val userInfo = db.collection("users").document(token)
                    userInfo.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                setTextFields(document)
                                Log.d(ContentValues.TAG, "UserInfo data: ${document.data}")
                            } else {
                                Log.d(ContentValues.TAG, "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d(ContentValues.TAG, "get failed with ", exception)
                        }
                }
            }
            loadImageFromStorage()
        } else {
            val userRef = db.collection("users").document(userId!!)
            userRef.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(ContentValues.TAG, "Listen failed", e)
                    return@addSnapshotListener
                }
                if (snapshot != null && snapshot.exists()) {
                    setTextFields(snapshot)
                }
            }

        }
    }

    private fun setTextFields(document: DocumentSnapshot) {
        var name = document.get("fullName").toString()
        var nickname = document.get("nickname").toString()
        var email = document.get("email").toString()
        var location = document.get("location").toString()
        var rate= document.get("rate").toString()
        var numrate=document.get("numrating").toString()

        if (name == "null") name = "Full Name"
        if (nickname == "null") nickname = "Nickname"
        if (email == "null") email = "email@example.com"
        if (location == "null") location = "Location"
        if(rate=="null" || rate=="0" || rate=="0.0") rate="Rating: 0"
        if(numrate=="null" ||numrate=="0") numrate="Number of ratings: 0"

        rate = "Rating: $rate"

        nameField.text = name
        nicknameField.text = nickname
        emailField.text = email
        locationField.text = location
        rateField.text = rate
        numrateField.text = numrate

    }

    private fun loadImageFromStorage() {
        try {
            val cw = ContextWrapper(requireActivity())
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            val f = File(directory, "profile.jpg")
            val b = BitmapFactory.decodeStream(FileInputStream(f))
            val img = requireView().findViewById<ImageView>(R.id.UserProfilePicture)
            img.setImageBitmap(b)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_profile, menu)
    }
}