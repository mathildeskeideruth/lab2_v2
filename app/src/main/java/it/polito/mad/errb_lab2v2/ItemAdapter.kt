package it.polito.mad.errb_lab2v2

import android.content.ContentValues.TAG
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

interface OnItemClickListener {
    fun onItemClick(item: Item)
    fun onEditClick(item: Item)

}

class ItemAdapter(var items: MutableList<Item>,
                  private val listener: OnItemClickListener?,
                  val fragment : Int
) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    override fun getItemCount(): Int = items.size

    fun getItem(position: Int): Item {
        return items[position]
    }


    fun getItemFromId(id: String): Item? {
        for (item in items) {
            if (item.id == id) return item
        }
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        if (listener != null) {
            holder.bind(items[position], listener, fragment)
        }
    }

    override fun onViewRecycled(holder: ItemViewHolder) {
        holder.unbind()
    }


    //------------------------------ ItemViewHolder ----------------------------
    class ItemViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {
        private val storage = Firebase.storage

        private var title: TextView? = null
        private var desc: TextView? = null
        private var price: TextView? = null
        private var location: TextView? = null
        private var category: TextView? = null
        private var expDate: TextView? = null
        private var image: ImageView? = null
        private var editBtn: Button
        private var rateBtn: Button
        private var soldTextView: TextView? = null
        private var hiddenTextView: TextView? = null
        private var ratedTextView: TextView? = null


        init {
            title = itemView.findViewById(R.id.item_title)
//            desc = itemView.findViewById(R.id.item_desc)
            price = itemView.findViewById(R.id.item_price)
            location = itemView.findViewById(R.id.item_loc)
            category = itemView.findViewById(R.id.item_cat)
//            expDate = itemView.findViewById(R.id.item_expDate)
            image = itemView.findViewById(R.id.item_list_image)
            editBtn = itemView.findViewById(R.id.edit)
            rateBtn=itemView.findViewById(R.id.rate)
            soldTextView = itemView.findViewById(R.id.item_sold)
            hiddenTextView = itemView.findViewById(R.id.item_hidden)
            ratedTextView=itemView.findViewById(R.id.item_rated)
        }

        fun bind(i: Item, listener: OnItemClickListener, fragment: Int) {
            title?.text = i.title
            desc?.text = i.description
            price?.text = i.price
            location?.text = i.location
            category?.text = i.category
            expDate?.text = i.expDate

            loadImage(i)

            if (i.isSold) soldTextView?.visibility = View.VISIBLE
            if (i.isHidden) hiddenTextView?.visibility = View.VISIBLE

            if (fragment == 1){//Fragment is itemListFragment
                editBtn.visibility = View.VISIBLE
            }
            if (fragment == 2 || fragment == 3 || fragment == 4) {//Fragment is not itemListFragment
                editBtn.visibility = View.INVISIBLE
            }
            if(fragment==1 || fragment==2 ||fragment==3 ||i.isRated){//where it should not be able to rate a seller
                rateBtn.visibility=View.INVISIBLE
            }

            itemView.setOnClickListener {
                listener.onItemClick(i)
                Log.d(TAG, itemId.toString())
                val bundle = bundleOf("itemId" to i.id)
                if (fragment == 1){//Fragment is ItemListFragment
                    bundle.putInt("previous", 1)
                    Navigation.findNavController(itemView)
                    .navigate(R.id.action_itemListFragment_to_itemDetailsFragment, bundle)
                }
                if (fragment == 2) {//Fragment is onSaleListFragment
                    bundle.putInt("previous", 2)
                    Navigation.findNavController(itemView)
                        .navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, bundle)
                }
                if (fragment == 3) {//Fragment is itemOfInterestFragment
                    bundle.putInt("previous", 3)
                    Navigation.findNavController(itemView)
                        .navigate(R.id.action_itemOfInterestFragment_to_itemDetailsFragment, bundle)
                }
                if (fragment == 4) {//Fragment is boughtItemsListFragment
                    bundle.putInt("previous", 4)
                    Navigation.findNavController(itemView)
                        .navigate(R.id.action_boughtItemsListFragment_to_itemDetailsFragment, bundle)
                }
            }
            editBtn.setOnClickListener {
                listener.onEditClick(i)
                val bundle = bundleOf("itemId" to i.id)
                Navigation.findNavController(itemView)
                    .navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
            }
            rateBtn.setOnClickListener {
                if(!i.isRated) {

                    val bundle = bundleOf("itemId" to i.id)
                    bundle.putInt("previous", 10)
                    Navigation.findNavController(itemView)
                        .navigate(
                            R.id.action_boughtItemsListFragment_to_itemDetailsFragment,
                            bundle
                        )
                }
            }
        }

        fun unbind() {
            editBtn.setOnClickListener(null)
        }

        private fun loadImage(item: Item) {
            if (image != null) {
                val itemRef = storage.reference.child("${item.id}.jpg")

                val ONE_MEGABYTE: Long = 1024 * 1024
                itemRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size, options)
                    image!!.setImageBitmap(bitmap)
                }.addOnFailureListener {
                    Log.d(TAG, "!!!!!!!!! CANT DOWNLOAD IMAGES !!!!!!!!!")
                }
            }
        }
    }



    //-------------------------------- New items handling ---------------------------------------

    fun updateListItems(insertItems: ArrayList<Item>) {

        val diffUtilCallback = ItemDiffUtilCallback(items, insertItems)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback)

        items = insertItems
        diffResult.dispatchUpdatesTo(this)
    }
}

//----------------------------------- ItemDiffUtilCallback --------------------------------------

class ItemDiffUtilCallback(
    private val oldItems: List<Item>,
    private val newItems: List<Item>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].id == newItems[newItemPosition].id
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val (_, title, desc, price, loc, cat, exp, imageURL) = oldItems[oldItemPosition]
        val (_, title1, desc1, price1, loc1, cat1, exp1, imageURL1) = newItems[newItemPosition]

        return title == title1
                && desc == desc1
                && price == price1
                && loc == loc1
                && cat == cat1
                && exp == exp1
                && imageURL == imageURL1
    }

}



